package Evaluador;
import ufps.util.colecciones_seed.*;
/**
 *
 * @author Juan Esteban
 */
public class Expresion {
    
  private ListaCD<String> expresiones=new ListaCD();  

    public Expresion() {
    }
    
    public Expresion(String cadena) {
        
       String v[]=cadena.split(",");
       for(String dato:v) 
           this.expresiones.insertarAlFinal(dato);
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
     String msg="";
     for(String dato:this.expresiones)
         msg+=dato+"<->";
    return msg;
    }
    
    public String getPrefijo()
    {
        //https://www.youtube.com/watch?v=kzuplp4NVvY
        ListaCD<Character> expInv = invertirExpresion();
        Pila<Character> pila = new Pila();
        String prefijo = "";
        String prefijoInv = "";
        
        for(char dato:expInv)
        {            
            if(dato == '/' || dato == '*' || dato == '-' || dato == '+' || dato == ')')
            {
                pila.apilar(dato);
            }
            if(dato != '/' && dato != '*' && dato != '-' && dato != '+' && dato != ')' && dato != '(')
            {
                prefijo = prefijo + dato;
            }
            if(dato == '(')
            {
                char valorTope = 'f';
                while(valorTope != ')')
                    {
                        valorTope = pila.desapilar();
                        if(valorTope == '/' || valorTope == '*' || valorTope == '-' || valorTope == '+')
                            prefijo = prefijo + valorTope;
                    }
            }
            
        }
        
        while(!pila.esVacia())
        {
            prefijo = prefijo + pila.desapilar();           
        }
        //inversion de la expresion
        for(int i = prefijo.length()-1;i>=0;i--)
        {
            prefijoInv = prefijoInv + prefijo.charAt(i);
        }
        //System.out.print(prefijoInv + "\n");
        return prefijoInv;
    }
    
    
    public String getPosfijo()
    {
        return "";
    }
    
    
    public float getEvaluarPosfijo()
    {
        return 0.0f;
    }
    
    private ListaCD<Character> invertirExpresion()
    {
        if(this.expresiones.esVacia())
        {
            return null;
        }
        
        ListaCD<Character> expInv = new ListaCD();
        String expresion = "";
        for(String dato:this.expresiones)
        {
            expresion = expresion + dato;
        }
        for(int i = expresion.length()-1;i>=0;i--)
        {
            expInv.insertarAlFinal(expresion.charAt(i));
        }
        return expInv;
    }
    
}
